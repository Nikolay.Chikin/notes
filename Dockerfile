FROM python:3.11-slim

WORKDIR /root

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY lists lists
COPY notes notes
COPY manage.py manage.py
COPY secret_key.txt secret_key.txt

RUN python3 manage.py makemigrations lists
RUN python3 manage.py migrate

ENTRYPOINT ["python3", "manage.py", "runserver", "0.0.0.0:80"]
