from django.urls import path
from . import views

urlpatterns = [
    path("", views.Root.as_view(), name="root"),
    path("list/<uuid:id>", views.List.as_view(), name="list"),
]
