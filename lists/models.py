from django.db import models

class ListName(models.Model):
    uuid = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=128)

class ListItem(models.Model):
    uuid = models.ForeignKey(ListName, on_delete=models.CASCADE)
    text = models.CharField(max_length=128)
