from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse
from .forms import NewList, NewItem
from .models import ListName, ListItem
from uuid import uuid4


class Root(FormView):
    template_name = "root.html"
    form_class = NewList

    def form_valid(self, form):
        name = form.cleaned_data["name"]
        cur_list = ListName.objects.filter(name=name).first()
        if cur_list is not None:
            uuid = cur_list.uuid
        else:
            uuid = uuid4()
            ListName(uuid=uuid, name=name).save()
        return HttpResponseRedirect(reverse("list", args=[uuid]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["lists"] = ListName.objects.all()
        return context


class List(FormView):
    template_name = "list.html"
    form_class = NewItem

    def form_valid(self, form):
        uuid = self.kwargs["id"]
        text = form.cleaned_data["text"]
        ListItem(uuid_id=uuid, text=text).save()
        return HttpResponseRedirect(reverse("list", args=[uuid]))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        uuid = self.kwargs["id"]
        context["items"] = ListItem.objects.filter(uuid=uuid).all()
        return context
