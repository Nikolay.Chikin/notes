from django.forms import modelform_factory, TextInput
from .models import ListName, ListItem

NewList = modelform_factory(
    ListName,
    fields=["name"],
    labels={"name": ""},
    widgets={
        "name": TextInput(
            attrs={"placeholder": "List name", "style": "text-align: center"}
        )
    },
)

NewItem = modelform_factory(
    ListItem,
    fields=["text"],
    labels={"name": ""},
    widgets={
        "text": TextInput(
            attrs={"placeholder": "Item text", "style": "text-align: center"}
        )
    },
)
